## m3u-to-mp3 v1.0

Bash script which takes as input an m3u or m3u8 playlist file and collects to a given folder all the audio files that this playlist contains, if they exist.

---

## How it works

1. The first step of the script is to copy the original playlist, since it is going to edit the file through execution.
2. Removes the lines of the script which do not contain path to file
3. **(Optional)** In case the playlist was created under windows, please edit the script and uncomment line "sed -i -e 's/\\/\//g' $TEMPFILE"
4. **(Optional)** In case the playlist was created under windows, please edit the script and uncomment line "sed -i -e 's/G:\/Music/\/media\/nyxteridas\/My\ Disk\/Music/g' $TEMPFILE" with a suitable for you sed command. 
This command is replacing windows paths with linux paths
5. Escape empty space characters in paths
6. Create a folder (if does not exist) with current date under the output folder specified by user
7. Loop the playlist file and copy all the files one by one to the output folder
8. Remove temporary playlist file.