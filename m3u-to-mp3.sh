#!/bin/bash

# This script gets as input a playlist file of type m3u8 or m3u
# and copies the files which are included in it to a separate folder

i=0;
echo -e "Please insert the path to the original m3u/m3u8 file:"
read FILE
echo -e "Please insert the path in which you wish the temporary m3u/m3u8 file to be created:"
read TEMPFILE
echo -e "Please insert the path to output folder:"
read outputf

# create a temp list in order to avoid any corruption of the main file
if [ -f "$FILE" ]; then
  cp $FILE $TEMPFILE
fi  

# remove lines which do not contain path to files
if [ -f "$TEMPFILE" ]; then
  sed -i '/\#EXTINF/d' $TEMPFILE
  sed -i '/\#EXTM3U/d' $TEMPFILE

# in case the playlist was created under windows, please uncomment below line. It replaces backslashes with forward slashes
#  sed -i -e 's/\\/\//g' $TEMPFILE

# in case the playlist was created under windows, please uncomment below line. It makes windows paths suitable for linux processing
# you will need to create your own sed here.
# in the example below I am replacing the path G:/Music with /media/nyxteridas/My Disk/Music
#  sed -i -e 's/G:\/Music/\/media\/nyxteridas\/My\ Disk\/Music/g' $TEMPFILE

# put a backslash before every space
  sed -i -e 's/ /\\ /g' $TEMPFILE

# create folder in which we will copy our songs from playlist
  DATE=`date +%Y-%m-%d`
  folder=$outputf/$DATE

# check if the folder already exists

  if [ -d "$folder" ]; then
     echo "Folder $DATE found!"
  else
     mkdir $folder
  fi


# echo "################################"

  while read line;do

    line=${line%$'\r'}
  #echo "Line # $k: $line"

  # check if the file exists
    if [ -f "$line" ]; then
       cp "$line" $folder
       #echo "Found"
       ((i++))
    else
       echo "File $line not found or moved!"
    fi
  
  done < $TEMPFILE
  echo "Total files copied: $i"

  rm $TEMPFILE
fi

